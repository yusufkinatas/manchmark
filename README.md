# Manchmark

An ability test for human beings.

## Tests

### Number Memory
* En uzun sayıyı akılda tutma

### Verbal Memory
* Kısa süre içinde fazla sayıda kelimeyi akılda tutma

### Visual Memory
* Kartlar arasından renkleri farklı olanları akılda tutma

### Reaction Time
* Ekrandaki görsel değişime en kısa sürede tepki verme

### Calculation Speed
* Karşılaşılan matematik işlemlerini en kısa sürede ve doğru şeklide cevaplama

### Touch Speed
* En kısa sürede ekrana 100 kere dokunma

### Typing Speed
* Ekranda beliren kelimeleri en kısa sürede ve doğru şeklide yazma

## Developers

* [Yusuf Kınataş](https://www.linkedin.com/in/yusuf-kinatas/) - Lead Programmer
* [Yağız Akyüz](https://www.linkedin.com/in/yagiz-akyuz/) - Junior Frontend Developer

